CROSSTOOL="$(PWD)/deps/crosstool-NG"
CROSSTOOL_PATH="$(CROSSTOOL)/builds/xtensa-esp32-elf/bin"
INTERMEDIATE="$(PWD)/intermediates"
IDF_PATH="$(PWD)/deps/esp-idf"

crosstool:
	cd $(CROSSTOOL); ./bootstrap; \
		./configure --enable-local --prefix=$(INTERMEDIATE) --exec-prefix=$(INTERMEDIATES);
	echo 'Run `cd deps/crosstool-NG; make install`, then return and run `make crosstool-build`'

crosstool-build:
	cd $(CROSSTOOL); ./ct-ng xtensa-esp32-elf; ./ct-ng build

%:
	IDF_PATH=$(IDF_PATH) PATH=$(PATH):$(CROSSTOOL_PATH) $(MAKE) -C src $@
